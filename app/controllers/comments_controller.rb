class CommentsController < ApplicationController
  def create
  	params.permit!
  	@post = Post.find(params[:post_id])
  	@comment = @post.comments.build(params[:comment])
  	@comment.save

  	redirect_to @post
  end

  def destroy
  	params.permit!
  	@comment = Comment.find(params[:id])
  	@comment.destroy

  	redirect_to @comment.post
  end
end
